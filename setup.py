
import setuptools
import glob
##############################################

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    install_requires = fh.read()

script_list = []
for script in glob.glob("bin/*.py"):
    script_list.append(script)

setuptools.setup(
     name='PyMINEr',  
     version='0.5',
     author="Scott Tyler",
     author_email="scottyler89@gmail.com",
     description="PyMINEr: automated biologic insights from large datasets.",
     long_description=long_description,
     long_description_content_type="text/markdown",
     install_requires = install_requires,
     url="https://scottyler892@bitbucket.org/scottyler892/manaclust",
     packages=setuptools.find_packages(),
     scripts = script_list,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: GNU Affero General Public License v3",
         "Operating System :: OS Independent",
     ],
 )
