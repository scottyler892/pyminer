#!/usr/bin/env python3
import time
import os
import pickle
import sys
import json
import time
import argparse
from subprocess import Popen
from copy import deepcopy

# Python 2/3 adaptability
try:
    from urllib.parse import urlparse, urlencode
    from urllib.request import urlopen, Request
    from urllib.error import HTTPError
except ImportError:
    from urlparse import urlparse
    from urllib import urlencode
    from urllib2 import urlopen, Request, HTTPError
######################################################################
##############################################################
## basic function library
def read_file(tempFile,linesOraw='lines',quiet=False):
    if not quiet:
        print('reading',tempFile)
    f=open(tempFile,'r')
    if linesOraw=='lines':
        lines=f.readlines()
        for i in range(0,len(lines)):
            lines[i]=lines[i].strip('\n')
    elif linesOraw=='raw':
        lines=f.read()
    f.close()
    return(lines)

def make_file(contents,path):
    f=open(path,'w')
    if isinstance(contents,list):
        f.writelines(contents)
    elif isinstance(contents,str):
        f.write(contents)
    f.close()

    
def flatten_2D_table(table,delim):
    #print(type(table))
    if str(type(table))=="<class 'numpy.ndarray'>":
        out=[]
        for i in range(0,len(table)):
            out.append([])
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i]=delim.join(out[i])+'\n'
        return(out)
    else:
        for i in range(0,len(table)):
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j]=str(table[i][j])
            table[i]=delim.join(table[i])+'\n'
    #print(table[0])
        return(table)

def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def make_table(lines,delim, num_type = float):
    #print(num_type)
    for i in range(0,len(lines)):
        lines[i]=lines[i].strip()
        lines[i]=lines[i].split(delim)
        for j in range(0,len(lines[i])):
            try:
                float(lines[i][j])
            except:
                lines[i][j]=lines[i][j].replace('"','')
            else:
                if num_type == float:
                    lines[i][j]=float(lines[i][j])
                elif num_type == int:
                    lines[i][j]=int(float(lines[i][j]))
                else:
                    lines[i][j]=num_type(lines[i][j])
    return(lines)


def get_file_path(in_path):
    in_path = in_path.split('/')
    in_path = in_path[:-1]
    in_path = '/'.join(in_path)
    return(in_path+'/')


def read_table(file, sep='\t',num_type=float):
    return(make_table(read_file(file,'lines'),sep,num_type=num_type))
    

def write_table(table, out_file, sep = '\t'):
    make_file(flatten_2D_table(table,sep), out_file)
    

def import_dict(f):
    f=open(f,'rb')
    d=pickle.load(f)
    f.close()
    return(d)


def save_dict(d,path):
    f=open(path,'wb')
    pickle.dump(d,f)
    f.close()


def cmd(in_message, com=True):
    print(in_message)
    time.sleep(.25)
    if com:
        Popen(in_message,shell=True).communicate()
    else:
        Popen(in_message,shell=True)

##############################################################


######################################################################
## 

######################################################################

## the below block of code was built from:
## https://github.com/Ensembl/ensembl-rest/wiki/Example-Python-Client
class EnsemblRestClient(object):
    def __init__(self, server='http://rest.ensembl.org', reqs_per_sec=15):
        self.server = server
        self.reqs_per_sec = reqs_per_sec
        self.req_count = 0
        self.last_req = 0
    def perform_rest_action(self, endpoint, hdrs=None, params=None):
        if hdrs is None:
            hdrs = {}
        if 'Content-Type' not in hdrs:
            hdrs['Content-Type'] = 'application/json'
        if params:
            endpoint += '?' + urlencode(params)
        data = None
        print(endpoint)
        # check if we need to rate limit ourselves
        if self.req_count >= self.reqs_per_sec:
            delta = time.time() - self.last_req
            if delta < 1:
                time.sleep(1 - delta)
            self.last_req = time.time()
            self.req_count = 0
        try:
            request = Request(self.server + endpoint, headers=hdrs)
            response = urlopen(request)
            content = response.read()
            if content:
                data = json.loads(content)
            self.req_count += 1
        except HTTPError as e:
            # check if we are being rate limited by the server
            if e.code == 429:
                if 'Retry-After' in e.headers:
                    retry = e.headers['Retry-After']
                    time.sleep(float(retry))
                    self.perform_rest_action(endpoint, hdrs, params)
            else:
                sys.stderr.write('Request failed for {0}: Status code: {1.code} Reason: {1.reason}\n'.format(endpoint, e))
        return data
    def get_variants(self, species, symbol):
        genes = self.perform_rest_action(
            endpoint='/xrefs/symbol/{0}/{1}'.format(species, symbol), 
            params={'object_type': 'gene'}
        )
        if genes:
            stable_id = genes[0]['id']
            variants = self.perform_rest_action(
                '/overlap/id/{0}'.format(stable_id),
                params={'feature': 'variation'}
            )
            return variants
        return None


######################################################################


def run(species, symbol):
    ## help with resful: http://rest.ensembl.org/?content-type=text/html
    ## http://rest.ensembl.org/variation/human/rs1199776527
    ## http://rest.ensembl.org/overlap/region/human/7:140924917..140924917?feature=gene
    ## http://rest.ensembl.org/info/variation/consequence_types/
    ###### eqtl with result
    ## http://rest.ensembl.org/eqtl/variant_name/human/rs7683255
    ###### eqtl without result
    ## http://rest.ensembl.org/eqtl/variant_name/human/rs1310656119 
    client = EnsemblRestClient()
    variants = client.get_variants(species, symbol)
    if variants:
        for v in variants:
            print('{seq_region_name}:{start}-{end}:{strand} ==> {id} ({consequence_type})'.format(**v))

######################################################################

class variant_obj():
    def __init__(self,client,rsID):
        self.id = rsID
        self.genes = []
        self.all_locus_response = []
        self.var_is_bad = None
        self.mutation_type = None
        self.get_direct_cause(client,self.id)


    def is_bad_mutation(self,var_details):
        types_of_mutations_acceptable = ['3_prime_UTR_variant','5_prime_UTR_variant','coding_sequence_variant','feature_truncation','frameshift_variant','incomplete_terminal_codon_variant','inframe_deletion','inframe_insertion','mature_miRNA_variant','missense_variant','NMD_transcript_variant','non_coding_transcript_exon_variant','non_coding_transcript_variant','protein_altering_variant','splice_acceptor_variant','splice_donor_variant','splice_region_variant','start_lost','start_retained_variant','stop_gained','stop_lost','stop_retained_variant','transcript_ablation','transcript_amplification']
        if "most_severe_consequence" in var_details:
            self.mutation_type = var_details["most_severe_consequence"]
            if self.mutation_type in types_of_mutations_acceptable:
                return(True)
        return(False)


    def get_gene_from_var(self,client, var_details):
        genes = []
        self.all_locus_response = []
        ## one approach is to get the intervals and look that up 
        for locus in var_details["mappings"]:
            #print(locus)
            if locus["assembly_name"]=="GRCh38":
                temp_contig = locus["seq_region_name"]
                temp_start = locus["start"]
                if "end" in locus:
                    temp_end = locus["end"]
                else:
                    temp_end = deepcopy(temp_start)
                interval = [temp_start,temp_end]
                start = str(min(interval))
                end = str(max(interval))
                endpoint = "/overlap/region/human/"+temp_contig+":"+start+".."+end+"?feature=gene"
                print(endpoint)
                temp_locus_response = client.perform_rest_action(endpoint)#, params = "feature=gene")
                self.all_locus_response.append(temp_locus_response)
                print('\n',temp_locus_response)
                for entry in temp_locus_response:
                    if "gene_id" in entry:
                        genes.append(entry["gene_id"])
        genes = sorted(list(set(genes)))
        return(genes)


    def get_mutation_type(self,client,variant, tries = 3):
        endpoint = "/variation/human/"+variant
        var_details = client.perform_rest_action(endpoint = endpoint)
        if var_details == None and tries > 0:
            time.sleep(.25)
            self.get_mutation_type(client, variant, tries = tries-1)
            return
        print(var_details)
        self.var_is_bad = self.is_bad_mutation(var_details)
        if self.var_is_bad:
            self.genes = self.get_gene_from_var(client,var_details)
        else:
            self.genes = []
        return


    def get_direct_cause(self,client,variant):
        ## first figure out if it's in the bad mutation type
        self.get_mutation_type(client,variant)
        ## if it is, get the gene
        ## if it's not, get the tad genes
        return()

    def __str__(self):
        return("\t".join([self.id,str(self.genes),'\t',self.mutation_type]))

def get_tad_variants(all_variants, tad_file):
    for variant in all_variants:
        print(variant)
        if variant.genes == []:
            ## now we get the genes for the intergenic and intronic
            print("finding the TAD mapping")


def map_variants_to_genes(var_file, stringdb_dir, out_dir):
    ## check that the tad file is present
    tad_file = os.path.join(stringdb_dir,"XXXXXXXX")
    if not os.path.isfile(tad_file)
        print("we couldn't find the tad file!\n",tad_file)
        sys.exit()
    ## do the analysis
    print('\nreading variants')
    variants = read_file(var_file,'lines')
    ## start the ensembl restful client
    client = EnsemblRestClient()
    all_variants = []
    for variant in variants:
        print("\n\n",variant)
        all_variants.append(variant_obj(client,variant))
    all_variants = get_tad_variants(all_variants = all_variants, 
                                    tad_file = tad_file)
    variant_summary_table = summarize_all_variants(all_variants, out_dir)
    return()






#####################################################################

parser = argparse.ArgumentParser()

## global arguments
parser.add_argument(
    '-infile','-in','-i',
    dest='infile',
    help="the text file with an rsID list",
    type=str)

parser.add_argument(
    "-stringdb_dir",'-sdb',
    help='The directory containing the StringDB action files',
    type = str,
    default = '/usr/local/lib/cell_signals/')

parser.add_argument(
    '-out','-o',
    help="the output file containing the rsID mapping ",
    type=str)

args = parser.parse_args()
#####################################################################

def main(args):
    map_variants_to_genes(var_file = args.infile, 
                          stringdb_dir = args.stringdb_dir, 
                          out_dir = args.out)
    return

if __name__ == '__main__':
    main(args)