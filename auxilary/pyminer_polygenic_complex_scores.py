
#!/usr/bin/env python3

##import dependency libraries
import sys,time,glob,os,pickle,fileinput,argparse, random
from time import sleep
from subprocess import Popen
from operator import itemgetter
import gc, fileinput
import numpy as np
from gprofiler import GProfiler


#import pandas as pd
##############################################################
## basic function library
def read_file(tempFile,linesOraw='lines',quiet=False):
    if not quiet:
        print('reading',tempFile)
    f=open(tempFile,'r')
    if linesOraw=='lines':
        lines=f.readlines()
        for i in range(0,len(lines)):
            lines[i]=lines[i].strip('\n')
    elif linesOraw=='raw':
        lines=f.read()
    f.close()
    return(lines)

def make_file(contents,path):
    f=open(path,'w')
    if isinstance(contents,list):
        f.writelines(contents)
    elif isinstance(contents,str):
        f.write(contents)
    f.close()

    
def flatten_2D_table(table,delim):
    #print(type(table))
    if str(type(table))=="<class 'numpy.ndarray'>":
        out=[]
        for i in range(0,len(table)):
            out.append([])
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i]=delim.join(out[i])+'\n'
        return(out)
    else:
        for i in range(0,len(table)):
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j]=str(table[i][j])
            table[i]=delim.join(table[i])+'\n'
    #print(table[0])
        return(table)

def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def make_table(lines,delim):
    for i in range(0,len(lines)):
        lines[i]=lines[i].strip()
        lines[i]=lines[i].split(delim)
        for j in range(0,len(lines[i])):
            try:
                float(lines[i][j])
            except:
                lines[i][j]=lines[i][j].replace('"','')
            else:
                lines[i][j]=float(lines[i][j])
    return(lines)


def get_file_path(in_path):
    in_path = in_path.split('/')
    in_path = in_path[:-1]
    in_path = '/'.join(in_path)
    return(in_path+'/')


def read_table(file, sep='\t'):
    return(make_table(read_file(file,'lines'),sep))
    
def write_table(table, out_file, sep = '\t'):
    make_file(flatten_2D_table(table,sep), out_file)
    

def import_dict(f):
    f=open(f,'rb')
    d=pickle.load(f)
    f.close()
    return(d)

def save_dict(d,path):
    f=open(path,'wb')
    pickle.dump(d,f)
    f.close()

all_calls = []
def cmd(in_message, com=True):
    global all_calls, time, sleep
    if type(in_message)==list:
       in_message = ' '.join(in_message)
    print(in_message)
    all_calls.append(in_message)
    #time.sleep(.25)
    sleep(0.25)
    if com:
       Popen(in_message,shell=True).communicate()
    else:
       Popen(in_message,shell=True)

def check_infile(infile):
    if os.path.isfile(infile):
        return
    else:
        sys.exit(str('could not find '+infile))

def outfile_exists(outfile):
    if os.path.isfile(outfile):
        statinfo = os.stat(outfile)
        if statinfo.st_size!=0:
            return(True)
        else:
            return(False)
    else:
        return(False)
##############################################################

import argparse
parser = argparse.ArgumentParser()

parser.add_argument(
    "-gene_lists",
    help='a comma separated list of files that contain the enriched genes for each cell type.',
    type = str)

parser.add_argument(
    "-ID_list","-ids",
    help = 'the original id list',
    default = "/home/scott/Documents/others/lauren/prelim_run/pyminer/ID_list.txt",
    type = str)

parser.add_argument(
        '-annotation_table',
        help="The annotation_table generated by PyMINEr.",
        default = "/home/scott/Documents/others/lauren/prelim_run/pyminer/annotations.tsv",
        type=str)

parser.add_argument(
    "-organism",'-species','-s',
    help='which organism are we using. (Use the -species_codes to print out a list of all supported species and their codes).',
    type = str,
    default = 'hsapiens')

parser.add_argument(
    "-stringdb_dir",'-sdb',
    help='The directory containing the StringDB action files',
    type = str,
    default = '/usr/local/lib/cell_signals/')

args = parser.parse_args()
##############################################################
##############################################################


#########
# ## first we'll load into memory only the interactions that stand
# ## a chance of being pertinent to our dataset
# print('\n\nloading in the interaction table')
# action_table, activation_dict, inhibition_dict, other_dict, all_included_bg = subset_action_table(receptor_or_secreted_list,db_file)
# print(len(action_table)-1,'interactions among the genes remaining')


def clean_names(in_vect):
    name1 = in_vect[0].split('.')
    name2 = in_vect[1].split('.')
    name1 = name1[-1]
    name2 = name2[-1]
    in_vect[0] = name1
    in_vect[1] = name2
    return(in_vect)

def convert_ensp_to_ensg(in_ids, organism):
    id_lookup_dict = {value:None for key, value in enumerate(in_ids)}
    gp = GProfiler('PyMINEr_'+str(random.randint(0,int(1e6))), want_header = True)
    results = gp.gconvert(in_ids, 
        organism = organism, target='ENSG')
    for r in range(1,len(results)):
        g = in_ids[results[r][0]-1]## look it up by the index
        ## the current orthologue list
        id_lookup_dict[g] = results[r][3]
    return(id_lookup_dict)



def get_all_ids(db_file, organism, binding_only = True):
    ## first convert to ensg
    print('converting stringDB file to an ensg adjacency matrix')
    all_ids = []
    line_count = 0
    out_table = []
    activation_dict = {}
    inhibition_dict = {}
    other_dict = {}
    first = True
    print(db_file)
    for line in fileinput.input(db_file):
        line_count+=1
        if line_count % 1e5 == 0:
            print('\t',line_count,'\t',len(out_table))
        temp_line = strip_split(line)
        if first:
            first = False
            #out_table.append(temp_line) 
        else:
            temp_line = clean_names(temp_line)
            all_ids += temp_line[:2]
    fileinput.close()
    all_ids = list(set(all_ids))
    return(all_ids)



def get_interaction_adj_mat(db_file, in_both_ensg_list, in_both_ensg_index_hash, id_converstion_dict, binding_only = True):
    ## first convert to ensg
    print('converting stringDB file to an ensg adjacency matrix')
    all_ids = []
    line_count = 0
    out_table = []
    activation_dict = {}
    inhibition_dict = {}
    other_dict = {}
    first = True
    string_db_master_adj_mat = np.array(np.zeros((len(in_both_ensg_list),len(in_both_ensg_list))),dtype=bool)
    print(db_file)
    for line in fileinput.input(db_file):
        line_count+=1
        if line_count % 1e5 == 0:
            print('\t',line_count,'\t',len(out_table))
        temp_line = strip_split(line)
        if first:
            first = False
            #out_table.append(temp_line) 
        else:
            temp_line = clean_names(temp_line)
            ## log the interaction
            is_binding = temp_line[2] == 'binding'
            is_binding_or_activating = (is_binding or temp_line[2] == 'activating')
            if binding_only and is_binding or (not binding_only):
                ## get the IDs
                ensg_id_1 = id_converstion_dict[temp_line[0]]
                ensg_id_2 = id_converstion_dict[temp_line[1]]
                ## check that both are in the final ensg set
                if ensg_id_1 in in_both_ensg_index_hash and ensg_id_2 in in_both_ensg_index_hash:
                    ## get the indices for the ids
                    idx1 = in_both_ensg_index_hash[ensg_id_1]
                    idx2 = in_both_ensg_index_hash[ensg_id_2]
                    string_db_master_adj_mat[idx1,idx2] = True
                    string_db_master_adj_mat[idx2,idx1] = True

    fileinput.close()
    all_ids = list(set(all_ids))
    return(all_ids)

def convert_string_db_to_ensg_adj_mat(db_file, organism, dataset_ensg_set, binding_only = True):
    all_ensp_ids = get_all_ids(db_file, organism, binding_only = binding_only)

    ## figure out which ensg IDs are present both in the stringDB and given datasets

    id_converstion_dict = convert_ensp_to_ensg(all_ensp_ids, organism)
    string_db_ensg_set = set(id_converstion_dict.values())
    in_both_ensg_set = string_db_ensg_set.intersection(dataset_ensg_set)
    in_both_ensg_list = sorted(list(in_both_ensg_set))
    in_both_ensg_index_hash = {value:key for key, value in enumerate(in_both_ensg_list)}
    print(len(in_both_ensg_list), "ENSG IDs found in both the input dataset and StringDB")

    string_db_master_adj_mat = get_interaction_adj_mat(db_file, in_both_ensg_list, in_both_ensg_index_hash, id_converstion_dict, binding_only = binding_only)

    ############ next we'll have to subset the interaction matrix for only what's expressed (in both dimentions)
    sys.exit()

def get_original_to_ensg_dict(ID_list_file, annotation_table):
    ## get the dataset's ENSG id dictionary
    ID_list = read_file(ID_list_file,'lines')
    annotation_table = read_table(annotation_table)
    ensg_dict = {value:None for key, value in enumerate(ID_list)}
    ensg_list = []
    for i in range(1,len(annotation_table)):
        original_index = int(annotation_table[i][0]-1)
        g = ID_list[original_index]
        ensg_dict[g]= annotation_table[i][3]
        ensg_list.append(annotation_table[i][3])
    ensg_set = set(ensg_list)
    return(ensg_dict, ensg_set)

def get_cell_type_disease_complexes(string_db_dir, 
                                    organism, 
                                    annotation_table, 
                                    ID_list_file):
    organism_action_files = {
        'hsapiens':'9606.protein.actions.v11.0.txt',
        'mmusculus':'10090.protein.actions.v11.0.txt'
    }
    string_db_file = string_db_dir+"/"+organism_action_files[organism]

    original_to_ensg_dict, dataset_ensg_set = get_original_to_ensg_dict(ID_list_file, annotation_table)


    ## get the interaction matrix
    interaction_matrix = convert_string_db_to_ensg_adj_mat(string_db_file, organism, dataset_ensg_set)



## first create the interaction matrix from stringDB
if __name__ == '__main__':
    get_cell_type_disease_complexes(args.stringdb_dir, 
                                    args.organism,
                                    args.annotation_table,
                                    args.ID_list)




