#!/usr/bin/env python3
import argparse
import fileinput
import os
import sys
from copy import deepcopy
import numpy as np
#####################################################################

## basic function library
def read_file(tempFile,linesOraw='lines',quiet=False):
    if not quiet:
        print('reading',tempFile)
    f=open(tempFile,'r')
    if linesOraw=='lines':
        lines=f.readlines()
        for i in range(0,len(lines)):
            lines[i]=lines[i].strip('\n')
    elif linesOraw=='raw':
        lines=f.read()
    f.close()
    return(lines)

def make_file(contents,path):
    f=open(path,'w')
    if isinstance(contents,list):
        f.writelines(contents)
    elif isinstance(contents,str):
        f.write(contents)
    f.close()

    
def flatten_2D_table(table,delim):
    #print(type(table))
    if str(type(table))=="<class 'numpy.ndarray'>":
        out=[]
        for i in range(0,len(table)):
            out.append([])
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i]=delim.join(out[i])+'\n'
        return(out)
    else:
        for i in range(0,len(table)):
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j]=str(table[i][j])
            table[i]=delim.join(table[i])+'\n'
    #print(table[0])
        return(table)

def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def make_table(lines,delim, num_type = float):
    #print(num_type)
    for i in range(0,len(lines)):
        lines[i]=lines[i].strip()
        lines[i]=lines[i].split(delim)
        for j in range(0,len(lines[i])):
            try:
                float(lines[i][j])
            except:
                lines[i][j]=lines[i][j].replace('"','')
            else:
                if num_type == float:
                    lines[i][j]=float(lines[i][j])
                elif num_type == int:
                    lines[i][j]=int(float(lines[i][j]))
                else:
                    lines[i][j]=num_type(lines[i][j])
    return(lines)


def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def get_file_path(in_path):
    in_path = in_path.split('/')
    in_path = in_path[:-1]
    in_path = '/'.join(in_path)
    return(in_path+'/')

def read_table(file, sep='\t',num_type=float):
    return(make_table(read_file(file,'lines'),sep,num_type=num_type))
    
def write_table(table, out_file, sep = '\t'):
    make_file(flatten_2D_table(table,sep), out_file)

#####################################################################

parser = argparse.ArgumentParser()

parser.add_argument(
    "-stringdb_dir",'-sdb',
    help='The directory containing the StringDB action files. Note that this HAS to be the sorted bed file of CTFC/cohesin binding sites.',
    type = str,
    default = '~/bin/pyminer/lib/')

args = parser.parse_args()
#####################################################################

def get_contigs(in_file):
    temp_contigs = []
    first = True
    for line in fileinput.input(in_file):
        if first:
            first = False
        else:
            temp_line = strip_split(line)
            if temp_line[0] not in temp_contigs:
                temp_contigs.append(temp_line[0])
    fileinput.close()
    print("found these contigs:")
    for contig in temp_contigs:
        print("\t",contig)
    return(temp_contigs)


def populate_contig_dict(contig_dict, in_file):
    all_binding_sites = read_table(in_file)
    for i in range(1,len(all_binding_sites)):
        site = all_binding_sites[i]
        temp_cont = contig_dict[site[0]]
        temp_cont.append(site)
        contig_dict[site[0]] = temp_cont
    return(contig_dict)


def filter_binding_site_dict(contig_dict, verbose = False):
    ## go through and figure out where the boundaries are
    all_contigs = sorted(list(contig_dict.keys()))
    rearranged_contig_dict = {}
    for contig in all_contigs:
        print("rearranging",contig)
        rearranged_contig = []
        
        ## figure out if we're starting with an insulator site or genes 
        temp_cont = contig_dict[contig]
        if temp_cont[0][-1] == "gene":
            in_genes = True
        else:
            in_genes = False

        ## set up the loop
        cur_elements = []## this will hold the temporary elements of the insulator or gene set
        previous_locus = 0
        for i in range(len(temp_cont)):
            ## condition to find switch
            if in_genes:
                if temp_cont[i][-1]=="gene":
                    ## in genes & continuing
                    cur_elements.append(temp_cont[i])
                else:
                    if verbose:
                        print('\n')
                        for el in cur_elements:
                            print('\t',el)
                    ## in genes & not continuing
                    rearranged_contig.append(cur_elements)
                    ## re-set and 
                    in_genes = False
                    cur_elements = [temp_cont[i]]
            if not in_genes:
                if temp_cont[i][-1]!="gene":
                    ## not in genes and continuing
                    cur_elements.append(temp_cont[i])
                else:
                    if verbose:
                        print('\n')
                        for el in cur_elements:
                            print('\t',el)
                    ## not in genes and not continuing
                    rearranged_contig.append(cur_elements)
                    ## change_in_genes to True
                    in_genes = True
                    cur_elements = [temp_cont[i]]
        rearranged_contig_dict[contig] = rearranged_contig
    return(rearranged_contig_dict)


class locus():
    def __init__(self,
                 locus_clump):
        self.contig=""
        self.start = -9999
        self.end = -9999
        self.genes = []
        self.annotations = ""
        self.locus_type = ""
        ## for genes we'll set the gene list in the first pass, but not the boundaries
        ## for insulator sites, we'll set the boundaries, but not the genes in the first pass
        ## this variable will help us keep track of whether or not we're finished processing the given locus
        self.finished = False
        self.process_locus(locus_clump)


    def process_locus(self, locus_clump):
        self.contig = locus_clump[0][0]
        if locus_clump[0][-1] == "gene":
            self.locus_type = "gene"
        else:
            self.locus_type = "insulator"
        
        if self.locus_type == "gene":
            ## here we 
            self.process_gene(locus_clump)
        else:
            ##
            self.process_insulator(locus_clump)
        self.add_annotations(locus_clump)


    def add_annotations(self,locus_clump):
        annotation_list = []
        for i in range(len(locus_clump)):
            annotation_list.append(','.join(list(map(str,locus_clump[i]))))
        annotation_list = '|'.join(annotation_list)
        self.annotations = annotation_list
        return


    def process_gene(self, locus_clump):
        ## we won't set the boundaries, just the genes
        for i in range(len(locus_clump)):
            self.genes.append(locus_clump[i][3])
        return


    def process_insulator(self, locus_clump):
        ## we won't set the genes, just the boundaries
        all_positions = []
        for i in range(0,len(locus_clump)):
            #print(locus_clump[i])
            all_positions.append(locus_clump[i][1])
            all_positions.append(locus_clump[i][2])
        self.start = min(all_positions)
        self.end = max(all_positions)

    def set_gene_loci(self, upstream = None, downstream = None):
        ## check that upstream and downstream are insulators
        ## if not, print descriptive error
        if upstream == None:
            self.start = 0
        else:
            self.start = upstream.end+1
        if downstream == None:
            self.end = 99999999999
        else:
            self.end = downstream.start-1
        self.finished = True
        return()


    def set_insulator_genes(self, upstream = None, downstream = None):
        if self.locus_type == "gene":
            return
        ## now we look for genes in the upstream and downstream if they exist & set our current genes to these
        if upstream != None:
            if upstream.locus_type == "gene":
                self.genes += upstream.genes
            else:
                sys.exit("we got an upstream insulator site next to another insulator site")
        else:
            self.start = 0
        if downstream != None:
            if downstream.locus_type == "gene":
                self.genes += downstream.genes
            else:
                sys.exit("we got an downstream insulator site next to another insulator site")
        else:
            self.end = 99999999999
        self.finished = True
        return


    def update_up_and_down(self, upstream = None, downstream = None):
        if self.locus_type == "gene":
            self.set_gene_loci(upstream = upstream, downstream = downstream)
        else:
            self.set_insulator_genes(upstream = upstream, downstream = downstream)
        return


    def get_output_line(self):
        ## contig, start, end, genes, annotatons
        out_line = [self.contig, str(int(self.start)), str(int(self.end)), self.locus_type, str(','.join(deepcopy(self.genes))), self.annotations] 
        return(out_line)   

    def __str__(self):
        return('\t'.join(self.get_output_line()))


###########################################################
class contig_obj():
    def __init__(self, name, list_of_locus_clumps):
        self.name = name
        self.contig_tads = []
        self.process_contig(list_of_locus_clumps)
        self.finalize_loci()
        self.prepare_for_lookup()

    def process_contig(self, list_of_locus_clumps):
        print("processing locus clumps for:",self.name)
        if len(list_of_locus_clumps) > 0:
            for element_set in list_of_locus_clumps:
                #print(element_set)
                self.contig_tads.append(locus(element_set))
        else:
            print('\tno genes or insulator sites in this contig; skipping')

    def finalize_loci(self):
        print("finalizing loci for",self.name)
        for i in range(0,len(self.contig_tads)):
            if i == 0:
                temp_upstream = None
            else:
                temp_upstream = self.contig_tads[i-1]
            if i == len(self.contig_tads)-1:
                temp_downstream = None
            else:
                temp_downstream = self.contig_tads[i+1]
            
            self.contig_tads[i].update_up_and_down(upstream = temp_upstream, downstream = temp_downstream)
            print('\n',self.contig_tads[i])

    def prepare_for_lookup(self):
        self.start_end_loci = np.zeros((len(self.contig_tads), 2))
        for i in range(len(self.contig_tads)):
            self.start_end_loci[i,0]=self.contig_tads[i].start
            self.start_end_loci[i,1]=self.contig_tads[i].end
        return

    def get_single_tad_idx_site(self, bp):
        """
        Takes in a single base number & gets the tad that contains it
        """
        abs_dist_mat = np.abs(self.start_end_loci-bp)
        min_abs_dist = np.min(abs_dist_mat, axis = 1)
        nearest_tad_idx = np.where(min_abs_dist == np.min(min_abs_dist))[0][0]
        return(nearest_tad_idx)


    def get_tad_for_site(self, bp_start, bp_end = None):
        """
        Takes a base pair start and end number as input and finds the tad locus that contains the input base pair number
        We allow for start and end sites for things like multi-base deltions, cnvs, etc that are not single point mutants
        """
        tad_idx_start = self.get_single_tad_idx_site(bp_start)
        if bp_end != None:
            tad_idx_end = self.get_single_tad_idx_site(bp_end)
        else:
            tad_idx_end = tad_idx_start
        ## plus one because the last tad idx is inclusive, while fancy subsetting is not inclusive of the last idx
        return(self.contig_tads[tad_idx_start:tad_idx_end+1])


    def get_genes_from_bp_site(self, bp_start, bp_end = None):
        tad_set = self.get_tad_for_site(bp_start = bp_start, bp_end = bp_end)
        temp_genes = []
        for tad in tad_set:
            temp_genes += tad.genes
        temp_genes = sorted(list(set(temp_genes)))
        return(temp_genes)

###########################################################

def process_rearranged_contigs_to_tads(contig_dict):
    all_contigs = sorted(list(contig_dict.keys()))
    tad_dict = {}
    ## go through each contig
    for contig in all_contigs:
        print(contig, len(contig_dict[contig]))
        tad_dict[contig] = contig_obj(contig, contig_dict[contig])
    return(tad_dict)


def process_bed(in_dir):
    in_file = os.path.expanduser(os.path.join(in_dir, "chip_tad_boundaries_hg38.bed"))
    if not os.path.isfile(in_file):
        print("in_file:",in_file)
        sys.exit("we coudn't find the input file!:"+in_file)
    #### we'll do a few passes
    ## first to make all the contigs
    contig_dict = {contig:[] for contig in get_contigs(in_file)}
    ## and populate it with the binding sites
    contig_dict = populate_contig_dict(contig_dict, in_file)
    ## rearrange into chunks of elements that belong together
    rearranged_contig_dict = filter_binding_site_dict(contig_dict)
    ## then we'll get the intervals of the tad boundaries
    tad_dict = process_rearranged_contigs_to_tads(rearranged_contig_dict)
    # ## As a test, we'll look up some loci
    # print("\n\n\nchr1:1,000,000\n")
    # for tad in tad_dict["chr1"].get_tad_for_site(1000000):
    #     print('\t',tad)
    # print("\n\n\nchr1:1,000,000:2,000,000\n")
    # for tad in tad_dict["chr1"].get_tad_for_site(1000000,2000000):
    #     print('\t',tad)

    # print("\n\ngenes:\n",tad_dict["chr1"].get_genes_from_bp_site(1000000))
    # print("\n\ngenes:\n",tad_dict["chr1"].get_genes_from_bp_site(1000000,2000000))

    ### write the output
    

#####################################################################

def main(args):
    process_bed(args.stringdb_dir)
    return

if __name__ == '__main__':
    main(args)